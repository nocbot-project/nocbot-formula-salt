#!/bin/bash

case $(hostnamectl | grep Operating | awk '{print $3}') in
    Debian )
        export pkg_mngr=apt-get
        export os_famaily=Debian
        ;;
    CentOS )
        export pkg_mngr=yum
        export os_famaily=Redhat
        ;;
    * )
        ;;
esac

# Needed for Debian based systems
export DEBIAN_FRONTEND=noninteractive

export PATH="$PATH:/vagrant/bin"

mkdir -p /etc/salt/master.d
echo 'autosign_file: /etc/salt/autosign.conf' > /etc/salt/master.d/autosign.conf

cat > /etc/salt/autosign.conf << AUTOSIGN_CONF
salt
minion-*
AUTOSIGN_CONF


/bin/cat > /etc/salt/master.d/pillar_engine.conf << ENGINE_CONF
master_tops:
  saltclass:
    path: /srv/saltclass

ext_pillar:
  - saltclass:
    - path: /srv/saltclass
ENGINE_CONF

/bin/cat > /etc/salt/master.d/fileserver.conf << FILESERVER
fileserver_backend:
  - roots
  - gitfs

gitfs_ref_types:
  - tag
  - branch

file_roots:
  devmode:
    - /srv/formulas/nocbot-formula-salt

gitfs_base: salt

FILESERVER

$pkg_mngr install -y git lsof python-pygit2 rsync salt-master

systemctl enable salt-master
systemctl restart salt-master
