#!/bin/bash

# salt-master has needs
# salt-minion has needs

case $(hostnamectl | grep Operating | awk '{print $3}') in
    Debian )
        export pkg_mngr=apt-get
        export os_famaily=Debian
        ;;
    # We prefer not support a RHEL OS, but the truth is, people are forced to use it.
    CentOS )
        export pkg_mngr=yum
        export os_famaily=Redhat
        yum install -y epel-release
        yum install -y https://repo.saltstack.com/yum/redhat/salt-repo-latest-2.el7.noarch.rpm
        yum clean expire-cache
        ;;
    * )
        ;;
esac

# Needed for Debian based systems
export DEBIAN_FRONTEND=noninteractive

export PATH="$PATH:/root/bin"

# We need to remove all traces of salt-minion to setup testing
# needrestart throws ugly warnings in the terminal so remove it for testing
$pkg_mngr remove -y needrestart salt-minion
rm -rf /etc/salt
rm -rf /var/log/salt
rm -rf /var/cache/salt

# Start Salt Master configuration
if [[ $(hostname -s) == 'salt' ]]; then
    chmod 700 /root/bin/salt-master.sh
    /root/bin/salt-master.sh
fi

# cronjob to run salt-minion approximately every 10 to 15 minutes
# Keep here in case schedule.conf gets messed up for some reason
/bin/cat > /etc/cron.d/minion-highstate << 'HIGHSTATE'
# SHELL=/bin/bash
# PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
# MAILTO=root
# HOME=/
# # Call a highstate randomly, between 10 and 15 minutes.
# */10 * * * * root sleep $(((${RANDOM} \%4) +1)); /usr/bin/salt-call state.highstate > /dev/null 2>&1
HIGHSTATE

mkdir -p /etc/salt/minion.d
echo 'saltenv: devmode' > /etc/salt/minion.d/saltenv.conf
echo "master: salt.$(hostname -d)" > /etc/salt/minion.d/master.conf
echo "id: $(hostname -s)" > /etc/salt/minion.d/id.conf

/bin/cat > /etc/salt/minion.d/schedule.conf << SCHEDULE
schedule:
  highstate:
    function: state.highstate
    minutes: 15
SCHEDULE

echo "startup_states: highstate" > /etc/salt/minion.d/startup_states.conf

if [[ $os_famaily == 'Debian' ]]; then
    $pkg_mngr update
fi

$pkg_mngr upgrade -y

$pkg_mngr install -y lsof salt-minion cachefilesd

# Setup cachedfilesd to run after install
if [[ $os_famaily == 'Debian' ]]; then
    sed -i -e 's/^#RUN.*$/RUN=yes/' /etc/default/cachefilesd
    sleep 2
fi

/bin/cat > /usr/local/bin/hs << HS
#!/bin/bash

salt-call state.highstate
HS

chmod 770 /usr/local/bin/hs

systemctl start cachefilesd
systemctl start salt-minion
