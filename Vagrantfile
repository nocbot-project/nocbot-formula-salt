# -*- mode: ruby -*-

# This Vagrantfile exists to test Nocbot Ansible Roles and
# Saltstack Formula on a developers workstation
# https://app.vagrantup.com/nocbot

# The Nocbot Project maintains code for Debian stable.
# The Nocbot Project maintains code for Debian oldstable.
# The Nocbot Project aspires to maintain code for Debian testing.
# Debian releases defined:
# https://wiki.debian.org/DebianReleases#Current_Releases.2FRepositories
# Obviously the testing release is somewhat stable, but no guarentees

# DNS for this domain works for the hostnames:
# salt, minion-0{1,5} out of the box.
domain = "nocbot.dev"
minion_name = "minion"

ip_addr = "172.16.0"

box = "nocbot/stretch64"
box_version = "9.8.0"

required_plugins = %w( salty-vagrant-grains vagrant-proxyconf )
required_plugins.each do |plugin|
    exec "vagrant plugin install #{plugin};vagrant #{ARGV.join(" ")}" unless Vagrant.has_plugin? plugin || ARGV[0] == 'plugin'
end

Vagrant.configure("2") do |config|

  # NFS is mandatory do not uncomment this
  # config.nfs.functional = false

  config.vm.define :salt, primary: true do |salt_master|
    salt_master.vm.box = "#{box}"
    salt_master.vm.box_version = "#{box_version}"
    salt_master.vm.host_name = "salt.#{domain}"
    salt_master.vm.network "private_network", ip: "#{ip_addr}.10"

    # Setup synced folders (If you have problmes with nfs, use sshfs)
    salt_master.vm.synced_folder ".", "/vagrant", disabled: true
    salt_master.vm.synced_folder "dev/scripts", "/root/bin", type: "nfs"
    salt_master.vm.synced_folder "..", "/srv/formulas", type: "nfs"
    # We doesn't use pillar.sls data, instead it uses saltclass for it's pillar data
    salt_master.vm.synced_folder "dev/saltclass", "/srv/saltclass", type: "nfs"

    # Keep most things in bootstrap.sh, just put vagranty things in this Vagrantfile
    salt_master.vm.provision "shell", path: "dev/scripts/bootstrap.sh"
  end

  MINION_COUNT = 2

  (1..MINION_COUNT).each do |minion_id|
    config.vm.define "#{minion_name}-0#{minion_id}" do |salt_minion|
      salt_minion.vm.box = "#{box}"
      salt_minion.vm.box_version = "#{box_version}"
      salt_minion.vm.host_name = "#{minion_name}-0#{minion_id}.#{domain}"
      salt_minion.vm.network "private_network", ip: "#{ip_addr}.1#{minion_id}"
      # Setup synced folders
      salt_minion.vm.synced_folder ".", "/vagrant", disabled: true

      # Keep most things in bootstrap.sh, just put vagranty things in this Vagrantfile
      salt_minion.vm.provision "shell", path: "dev/scripts/bootstrap.sh"
    end
  end
end
