{%- from "salt/map.jinja" import minion with context %}

{%- if minion.get('enabled', False) %}

{%- if grains.os_family == 'Debian' %}
salt_minion__policy-rc.d_present:
  file.managed:
    - name: /usr/sbin/policy-rc.d
    - mode: 0755
    - contents: |
        #!/bin/bash
        exit 101
    - replace: False
    - prereq:
      - pkg: salt_minion__pkg
{%- endif %}

salt_minion__pkg:
  pkg.installed:
    - name: salt-minion
    {%- if minion.version is defined %}
    - version: {{ minion.version }}{% if grains['os_family'] == 'Debian' %}+ds-1{% endif %}
    {%- endif %}
    - require_in:
        - service: salt_minion__service
    - watch_in:
        - service: salt_minion__service

{%- if grains.os_family == 'Debian' %}
salt_minion__policy-rc.d_absent:
  file.absent:
    - name: /usr/sbin/policy-rc.d
    - onchanges:
      - pkg: salt_minion__pkg
{%- endif %}

{%- endif %}
