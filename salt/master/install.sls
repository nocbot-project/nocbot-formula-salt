{%- from 'salt/map.jinja' import master with context %}

{%- if master.get('enabled', False) %}

{%- if grains.os_family == 'Debian' %}
salt_master__policy-rc.d_present:
  file.managed:
    - name: /usr/sbin/policy-rc.d
    - mode: 0755
    - contents: |
        #!/bin/bash
        exit 101
    - replace: False
    - prereq:
      - pkg: salt_master__pkg
{%- endif %}

salt_master__pkg:
  pkg.installed:
    - name: {{ master.pkg }}
    {%- if master.version is defined %}
    - version: {{ master.version }}{% if grains['os_family'] == 'Debian' %}+ds-1{% endif %}
    {%- endif %}
    - require_in:
        - service: salt_master__service
    - watch_in:
        - service: salt_master__service

{%- if grains.os_family == 'Debian' %}
salt_master__policy-rc.d_absent:
  file.absent:
    - name: /usr/sbin/policy-rc.d
    - onchanges:
      - pkg: salt_master__pkg
{%- endif %}
{%- endif %}