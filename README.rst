***********************
Salt Saltstack Formula
***********************

This formula is part of the `Mantoso Nocbot Project`_

.. note::

  - Nocbot SaltStack Formula are intended to be used with ``gitfs`` and ``saltclass``
  - This is an editorconfig_ enabled repository.
  - Formula and Salt Package Manger versioning follows `Semantic Versioning`_ guidelines.

You should never rely on a repository you do not control for your infrastructure management. **We strongly recommend forking this formula repository** into your own account to avoid unexpected changes to your infrastructure.

Our SaltStack Formulas are highly active repositories so pull new changes with care. Any additions you make to your fork can be easily sent back upstream with a quick pull request!

.. _Mantoso Nocbot Project: https://mantoso.tech/nocbot
.. _editorconfig: http://editorconfig.org
.. _Semantic Versioning: http://semver.org

Features At A Glance
====================

- Configures Salt Master
- Configures Salt Minion
- Configures Salt SSH

Available States
================

The following states are available in this Saltstack formula:

.. contents::
   :local:

``salt``
^^^^^^^^

Installs ``salt-master``, ``salt-minion`` and ``salt-ssh`` if enabled in saltclass data.

``salt.master``
^^^^^^^^^^^^^^^

Installs and configures ``salt-master``. This configuration relies heavily on ``gitfs``.

``salt.minion``
^^^^^^^^^^^^^^^

Installs and configurs ``salt-minion`` if enabled in pillar data.

``salt.salt_ssh``
^^^^^^^^^^^^^^^^^

Installs and configurs ``salt-ssh`` if enabled in pillar data. This state also configures the salt roster file.

Usage
=====

To use this formula include ``salt`` in your saltclass node or class pillar data:

.. code-block:: console

    pillars:
      salt:
        lookup:

See ``pillar.example`` for pillar data.

Documentation
=============

To learn how to install and update Mantoso SaltStack Formulas, consult the documentation available online at:

https://docs.mantoso.tech/saltstack/formulas/

Formula License
===============

.. code-block:: console

    Copyright (C) 2019, Nocbot Project

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
